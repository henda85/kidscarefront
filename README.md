Dans le cadre de la validation de mon diplôme professionnel "développeur web et web mobile", j'ai développé l'application "Kidscare" qui permet aux parents de s'entraider pour la garde des enfants gratuitement. Le concept est qu'un parent ayant besoin d'une prise en charge d'urgence pour son enfant (grève scolaire, professeur absent...) peut poster un message sur l'application. Et les parents disponibles peuvent se manifester pour proposer une solution.

## Fonctionnalités

- S’inscrire :
  permet de créer un compte en renseignant une adresse mail, un mot de passe et diverses informations utilisateurs (nom, numéro de téléphone, adresse, avatar…)
- Se connecter :
  Une fois son compte créé, permet de s’identifier pour profiter de toutes les fonctionnalités de la plateforme.
- Poster des messages pour communiquer avec d’autres parents.
- Supprimer un message qui lui appartient.
- Pouvoir lire la listes des messages.
- Un utilisateur peut ajouter ses enfants et leurs établissements, ils peuvent être gardés en cas de besoin par d’autres parents(utilisateurs inscrit sur l’application).

## Technos utilisées:

NodeJs, React, NextJs,Bootstrap, TypeScript et MySql

## Les maquettes

![wireframe kidscare](public/maquettepc.png)
![wireframe kidscare](public/maquettesmobile.png)

## Diagramme des classes

![Diagrame kidscare](public/diagrammeclass.png)

## Lien vers le back-end

(https://gitlab.com/henda85/kidscareback)
