export interface User {
  passWord?: string;
  userName?: string;
  userEmail?: string;
  userAdress?: string;
  userPhone?: string;
  userDisponibility?: number;
  userAvtar?: string;
  userId?: number;
  kids?: Kids[];
}
export interface Messages {
  text?: string;
  datetime?: Date;
  messageId?: number;
  auteur?: string;
  avatar?: string;
}
export interface Kids {
  kidName?: string;
  kidId?: number;
  classId?: number;
  schoolId?: String;
  classLevel?: String;
}
export interface ClassRoom {
  classLevel?: string;
  schoolId?: string;
  classId?: number;
}
