import { NextPage } from "next";
import { useSession } from "next-auth/react";
import Head from "next/head";
import React from "react";
import Navbar from "../components/Navbar";
import { Kids, User } from "../entities";
import {
  LocationMarkerIcon,
  MailIcon,
  PhoneIcon,
} from "@heroicons/react/solid";
import Footer from "../components/Footer";
import { Button } from "react-bootstrap";
import Link from "next/link";

import KidsList from "../components/KidsList";

const profile: NextPage = () => {
  const { data: session } = useSession();
  const user = session?.user as User;
  return (
    <div>
      <Head>
        <title>Profile</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />
      <div className="container">
        {session && (
          <div className="row">
            <div className=" col-md-6 ">
              <img className="avatar" src={user.userAvtar} alt="" />
              <h1 className="profileUserName">{user.userName}</h1>
            </div>

            <div className="col-md-6 pt-5 pr-5">
              <div className="row mb-3">
                <div className="col-1">
                  <LocationMarkerIcon className="icon" />
                </div>
                <div className="col-6 userprofile">{user.userAdress}</div>
              </div>
              <div className="row mb-3">
                <div className="col-1">
                  <MailIcon className="icon" />
                </div>
                <div className="col-6 userprofile">{user.userEmail}</div>
              </div>
              <div className="row mb-3 ">
                {" "}
                <div className="col-1">
                  <PhoneIcon className="icon" />
                </div>
                <div className="col-6 userprofile">{user.userPhone}</div>
              </div>
              <div>
                {user.userDisponibility ? (
                  <p className="disponible">Vous êtes désomais disponible</p>
                ) : (
                  <p className="notdiponible">
                    Vous êtes désomais non disponible
                  </p>
                )}
              </div>
            </div>
            <div className="w-100 devisiant px-2">
              <h2 className="pdevisiant">ENFANT(S)</h2>
            </div>
            <div className="col-md-6 ">
              <img src="kidsList.png" alt="" className="kidsImage img-fluid" />
            </div>
            <div className="col-md-6 pt-5 pr-5">
              <KidsList />
              <div className="">
                <Link href="ajoutkids">
                  <Button>Ajouter un enfants</Button>
                </Link>
              </div>
            </div>
          </div>
        )}
      </div>
      <Footer />
    </div>
  );
};

export default profile;
