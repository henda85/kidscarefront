import React, { useEffect, useState } from "react";
import { Messages, User } from "../entities";
import { useSession } from "next-auth/react";
import axios from "axios";
import { TrashIcon } from "@heroicons/react/solid";

function MessagesList() {
  const { data: session } = useSession();
  const user = session?.user as User;

  if (!session) return <p> {"Oops vous n'êtes pas connectés"}</p>;

  const [messages, setmessages] = useState([]);

  const loadData = async () => {
    const response = await axios.get("http://localhost:8000/api/message/");
    setmessages(response.data);
  };
  const handleDelete = async (id: any) => {
    await axios.delete("http://localhost:8000/api/message/" + id, {
      headers: { Authorization: `Bearer ${session.accessToken}` },
    });
  };

  useEffect(() => {
    loadData();
  }, [messages]);

  return (
    <div className="">
      {messages &&
        messages.map((message: Messages) => (
          <div key={message.messageId} className="container">
            <div className="row d-flex justify-content-center">
              <div className="col-2 justify-content-md-center  ">
                <img src={message.avatar} alt="" className="avatarmessage" />
                <p>{message.auteur}</p>
              </div>
              <div className="col-4">
                <div className="row justify-content-center">
                  <p>
                    {new Intl.DateTimeFormat(["ban", "id"]).format(
                      new Date(message.datetime)
                    )}
                  </p>

                  <p className=" ">{message.text}</p>
                </div>
                {user.userName == message.auteur && (
                  <div>
                    <TrashIcon
                      className="icon"
                      onClick={() => handleDelete(message.messageId)}
                    />
                  </div>
                )}
              </div>
            </div>
            <hr className="w-50 my-2" style={{ margin: "auto" }} />
          </div>
        ))}
    </div>
  );
}

export default MessagesList;
