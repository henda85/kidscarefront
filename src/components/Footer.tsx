import React from "react";

function Footer() {
  return (
    <div className="row ">
      <div className="footer  ">
        <div className="centerlogo ">
          <a href="/">
            <img src="./logo2.png" alt="" className="logo" />
          </a>
        </div>
        <div>
          <p className="test"> © HENDA NAJEH 2022</p>
        </div>
      </div>
    </div>
  );
}

export default Footer;
