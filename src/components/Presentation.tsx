import React from "react";

function Presentation() {
  return (
    <div className="container d-flex flex-column justify-content-center">
      <div className="row  ">
        <div className="col content-center ">
          <img
            src="./icons8-sac-d'argent-euro-50.png"
            alt=""
            className="sac-argent"
          />
          <p className="text-icon">Economie</p>
        </div>
        <div className="col content-center ">
          <img src="./icons8-love-64.png" alt="" />
          <p className="text-icon">Bienveillance</p>
        </div>
        <div className="col content-center">
          <img src="./icons8-time-64.png" alt="" />
          <p className="text-icon">Gain de temps</p>
        </div>
      </div>
      <p className="pindex">
        Créé par une maman pour répondre au casse-tête des modes de garde,
        Kidscare propose des solutions souples et réactives à chaque moment de
        la vie de l’enfant, à partir du maternelle. Baby-sitting occasionnel sur
        tout la journée, grêves des profetionnelles, sortie d’école, garde d’un
        ou de plusieurs enfants lors d’un événement spécial ou pendant les
        vacances, maitresse absente, enfant malade Kidscare s’adapte à tous vos
        besoins.
      </p>
    </div>
  );
}

export default Presentation;
