import axios from "axios";

import Router from "next/router";
import React from "react";
import { Form } from "react-bootstrap";
import { Controller, useForm } from "react-hook-form";
import { User } from "../entities";

function Register() {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const onSubmit = async (data: User) => {
    await axios.post(process.env.NEXT_PUBLIC_SERVER_URL + "/api/user", data);
    Router.push("/");
  };
  console.log(errors);
  return (
    <div>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <div className="row g-2">
          <div className="col-md-4">
            <Form.Group className="mb-3">
              <Form.Label>Name</Form.Label>
              <Controller
                name="userName"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <Form.Control
                    {...field}
                    type="text"
                    placeholder="Enter user name"
                  />
                )}
              />
            </Form.Group>
          </div>
          <div className="col-md-4">
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Controller
                name="userEmail"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <Form.Control
                    {...field}
                    type="email"
                    placeholder="Enter user email"
                  />
                )}
              />
            </Form.Group>
          </div>
        </div>
        <div className="row">
          <div className="col-md-8">
            <Form.Group className="mb-3">
              <Form.Label>Adress</Form.Label>
              <Controller
                name="userAdress"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <Form.Control
                    {...field}
                    type="text"
                    placeholder="Enter user adress"
                  />
                )}
              />
            </Form.Group>
          </div>
        </div>
        <div className="row g-2">
          <div className="col-md-4">
            <Form.Group className="mb-3">
              <Form.Label>Phone</Form.Label>
              <Controller
                name="userPhone"
                control={control}
                rules={{ required: true }}
                render={({ field }) => (
                  <Form.Control
                    {...field}
                    type="text"
                    placeholder="Enter user phone"
                  />
                )}
              />
            </Form.Group>
          </div>
          <div className="col-md-4">
            <Form.Group className="mb-3">
              <Form.Label>Disponibility</Form.Label>
              <Controller
                name="userDisponibility"
                control={control}
                render={({ field }) => (
                  <Form.Select {...field}>
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                  </Form.Select>
                )}
              />
            </Form.Group>
          </div>
        </div>
        <div className="row ">
          <div className="col-md-8">
            <Form.Group className="mb-3">
              <Form.Label>Avatar</Form.Label>
              <Controller
                name="userAvtar"
                control={control}
                render={({ field }) => (
                  <Form.Control
                    {...field}
                    type="text"
                    placeholder="Enter your url avatar"
                  />
                )}
              />
            </Form.Group>
          </div>
        </div>
        <div className="row ">
          <div className="col-md-8">
            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Controller
                name="passWord"
                control={control}
                rules={{ required: false }}
                render={({ field }) => (
                  <Form.Control
                    {...field}
                    type="password"
                    isInvalid={errors.password}
                    placeholder="Enter password"
                  />
                )}
              />
            </Form.Group>
          </div>
        </div>
        <input type="submit" />
      </Form>
    </div>
  );
}

export default Register;
