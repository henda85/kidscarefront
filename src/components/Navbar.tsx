import React from "react";
import { signIn, signOut, useSession } from "next-auth/react";
import { Button } from "react-bootstrap";
function Navbar() {
  const { data: session } = useSession();
  return (
    <div className="navflex">
      <div className="centerlogo">
        <a href="/">
          <img src="./logo2.png" alt="" className="logo" />
        </a>
      </div>
      <div>
        <ul className="nav justify-content-center">
          <li className="nav-item">
            <a className="nav-link active" aria-current="page" href="/">
              Home
            </a>
          </li>
          <li className="nav-item">
            {session && (
              <a className="nav-link" href="profile">
                Profile
              </a>
            )}
          </li>
          <li className="nav-item">
            {!session && (
              <a className="nav-link" href="register">
                Register
              </a>
            )}
          </li>
          <li className="nav-item">
            {session && (
              <Button className="btn" onClick={() => signOut()}>
                Logout
              </Button>
            )}
            {!session && <Button onClick={() => signIn()}>Login</Button>}
          </li>
        </ul>
      </div>
    </div>
  );
}
export default Navbar;
