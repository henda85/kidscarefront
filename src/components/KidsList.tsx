import React, { useEffect, useState } from "react";
import { Kids, User } from "../entities";
import { useSession } from "next-auth/react";
import axios from "axios";

function KidsList() {
  const { data: session } = useSession();
  const user = session?.user as User;

  const [kids, setKids] = useState(user.kids);

  const loadData = async () => {
    const response = await axios.get(
      "http://localhost:8000/api/kid/" + user.userId
    );
    setKids(response.data);
  };

  useEffect(() => {
    loadData();
  }, []);
  return (
    <div className="">
      {kids &&
        kids.map((kid: Kids) => (
          <div key={kid.kidId}>
            <p className="pkidlist">
              {kid.kidName} {kid.classLevel}
            </p>
            {/* id ecole{kid.schoolId} */}
          </div>
        ))}
    </div>
  );
}

export default KidsList;
